# Teste Web Designer

O material disponibilizado foi pensado na versão Desktop, contudo, faz parte do teste adaptar o projeto para ser responsivo, condizendo com as técnicas do "mobile first". Ao receber esse link para a realização do teste, você terá 03 dias para a conclusão do mesmo.

## O Projeto

O protótipo para a realização do teste foi elaborado no Figma. Após fazer o cadastro, essa ferramenta vai permitir a sua interação com os elementos em tela, para que você possa exportar e inspecionar propriedades de estilização.

https://www.figma.com/file/2nos0QAduql5tjXKFQRtj2/Teste-Web-Designer?node-id=0%3A1

## Apresentação e Estimativa

Após a análise do protótipo, faça a sua estimativa de horas do planejamento a ser realizado e envie um e-mail com o título **[Web Designer] Teste - Estimativa** para beawesome@waac.com.br com o link do seu git(caso não saiba utilizar o git, nos envie só o source em .rar ou .zip). Você pode estimar o esforço por seção, assim você divide a estratégia em pequenas partes.

Um exemplo:

- Seção 01: 5 horas
- Seção 02: 3 horas
- Seção 03: 2 horas
- Seção 04: 2 horas

Estimativa: 12 horas.

## Estratégia e Escopo de Tecnologias

Diante da finita quantidade de frameworks e bibliotecas de estilização open-source, fique a vontade para escolher as tecnologia a serem utilizadas para a realização do teste. Use o material-ui e sistema de grids que preferir.

## Processo de avaliação

1. Estratégia
2. Redibilidade de código
3. Padrões
4. Responsividade

## Finalização

Ao finalizar o teste, envie um e-mail com o título **[Web Designer] Teste - Finalizado** para beawesome@waac.com.br.
